//
//  JsonModel.swift
//  JSON
//
//  Created by Ardavan Izadiyar on 21/9/16.
//  Copyright © 2016 Ardavan Izadiyar. All rights reserved.
//

import Foundation
import UIKit

var Alert:Bool = false

class JSONModel {
    
    // MARK: - RESTful Server
    let pathGet = "http://localhost/RS/API/ApiUsers/login/"
    let pathPost = "http://localhost/RS/API/ApiUsers/login/?XRSAPI=123456"
    
    // MARK: - API key
    let apiKey = "?XRSAPI=123456"
    
    
    // HOW TO SENT EMAIL @ SIGN TO THE URL
    // MAYBE SHOULD CONVERT !?
    func GetJson(user username: String, pass password: String) {
        let request = URLRequest(url: URL(string: pathGet + "\(username)/\(password)" + apiKey)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request) { (Data, Response, Error) in
            if Error != nil {
                print(Error)
                return
            }
            
            let response = Response as! HTTPURLResponse
            
            switch response.statusCode {
            case 200:
                
                // Parsing JSON data
                if let data = Data {
                    thisUser = self.ParseJSON(data: data as NSData)
                    OperationQueue.main.addOperation({
                        print("Run in the queue")
                    })
                }
                
                print("Successful")
                
            case 404:
                print("ERROR: WRONG USER / PASS")
                
            default:
                break
            }

            
            
        }
        
        task.resume()

        
    }
    
    //................................................Here I added a function as a parameter that returns a UIViewController object, this function will be implemented where this function is being called
    //................................................................⬇️.........................................
    func PostJson(user username: String, pass password: String, getMeTheVC:()->(UIViewController)) -> Bool {
        //Then we parse what we obteined from that function to our controller type
        let myViewControllerObject = getMeTheVC() as! myViewController
        
        var request = URLRequest(url: URL(string: "http://localhost/RS/API/ApiUsers/login/?XRSAPI=123456")!)
        request.httpMethod = "POST"
        
        let postString = "username=\(username)&password=\(password)"
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { Data, Response, Error in
            
            if Error != nil {
                print(Error)
                return
            }
            
            let response = Response as! HTTPURLResponse
            
            switch response.statusCode {
            case 200:
                
                // Parsing JSON
                if let data = Data {
                    thisUser = self.ParseJSON(data: data as NSData)
                    OperationQueue.main.addOperation({
                        print("Run in the queue")
                        //And now we can use the object that is the same as the one in our navigation Controller queue
                        myViewControllerObject.alert.text = "success"
                    })
                }
                
                print("Successful from POST method")
                Alert = true
                
                
            case 404:
                print("ERROR: WRONG USER / PASS")
                Alert = false
                
            default:
                Alert = false
                break
            }

        }
        task.resume()
        return Alert
    }
    
    
    
    func ParseJSON(data: NSData) -> [UserJson] {
        var thisUser = [UserJson]()
        
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
            
            
            // Parse JSON data
            let json = jsonResult?["userInfo"] as! [AnyObject]
                
                for user in json {
                    let userInfo = UserJson()
                    
                    userInfo.ID = user["ID"] as! Int
                    userInfo.Name = user["NAME"] as! String
                    userInfo.LastName = user["LASTNAME"] as! String
                    userInfo.IC = user["IC"] as! String
                    userInfo.Email = user["EMAIL"] as! String
                    userInfo.Activation = user["ACTIVATION"] as! Bool
                    userInfo.Status = user["STATUS"] as! String
                    userInfo.Photo = user["PHOTO"] as! String
                    
                    thisUser.append(userInfo)
                    
                    
                }
            
            
        } catch {
            print(error)
        }
        
        return thisUser
        
    }
    
    
    
}
