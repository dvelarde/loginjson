//
//  ViewController.swift
//  JSON
//
//  Created by Ardavan Izadiyar on 21/9/16.
//  Copyright © 2016 Ardavan Izadiyar. All rights reserved.
//

import UIKit

// MARK: - Global vars
var thisUser = [UserJson]()


class myViewController: UIViewController {
    
    // MARK: - Model
    let JSON = JSONModel()
    
    @IBOutlet var alert: UILabel!
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    
    
    //Here I create the function that is the same type as the requested by PostJson function
    func getMyself() -> UIViewController
    {
        //I am returning this same UIViewController
        return self;
    }
    
    @IBAction func login(_ sender: UIButton) {
        //..............................................................and here I send that function as a parameter
        if JSON.PostJson(user: username.text!, pass: password.text!, getMeTheVC: getMyself) {
            
            // I want these lines run after parsing JSON 
            alert.text = "Yes"
            performSegue(withIdentifier: "welcome", sender: nil)
            
            
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        username.text = "a@a.com" //"6140612e636f6d"
        password.text = "aaaaaa"
        
        alert.text = ""
        
    }
    
}

